package org.bitbucket.plasticmoose.lwneuralnetworks.utils;

import org.bitbucket.plasticmoose.lwneuralnetworks.Layer;
import org.bitbucket.plasticmoose.lwneuralnetworks.NeuralNetwork;
import org.bitbucket.plasticmoose.lwneuralnetworks.neurons.Identity;
import org.bitbucket.plasticmoose.lwneuralnetworks.neurons.Neuron;
import org.bitbucket.plasticmoose.lwneuralnetworks.neurons.Sigmoid;

import java.util.ArrayList;

public class NeuralNetworkBuilder {
    private final ArrayList<Layer> layers = new ArrayList<>();

    public NeuralNetworkBuilder addSigmoidLayer(double[][] weights, double[] bias) {
        Neuron[] sigmoids = new Neuron[bias.length];
        for (int i = 0; i < sigmoids.length; i++) {
            sigmoids[i] = new Sigmoid(bias[i]);
        }
        addNeuronLayer(weights, sigmoids);
        return this;
    }

    public NeuralNetworkBuilder addNeuronLayer(double[][] weights, Neuron[] neurons) {
        addLayer(new Layer(weights, neurons));
        return this;
    }

    public NeuralNetworkBuilder addIdentityLayer(double[][] weights) {
        addLayer(new Layer(weights, Identity.ofSize(weights.length)));
        return this;
    }

    public NeuralNetworkBuilder addLayer(Layer layer) {
        layers.add(layer);
        return this;
    }

    public NeuralNetwork build() {
        Layer[] layers = new Layer[this.layers.size()];
        for (int i = 0; i < this.layers.size(); i++) {
            layers[i] = this.layers.get(i);
        }
        return new NeuralNetwork(layers);
    }
}
