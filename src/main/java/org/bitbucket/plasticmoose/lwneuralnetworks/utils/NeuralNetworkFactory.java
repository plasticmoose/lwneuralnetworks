package org.bitbucket.plasticmoose.lwneuralnetworks.utils;

import org.bitbucket.plasticmoose.lwneuralnetworks.NeuralNetwork;

public class NeuralNetworkFactory {
    private final RandomGenerator randomGenerator;

    public NeuralNetworkFactory(RandomGenerator randomGenerator) {
        this.randomGenerator = randomGenerator;
    }

    public NeuralNetwork getNetworkWithRandomWeights(int... layerSizes) {
        NeuralNetworkBuilder neuralNetworkBuilder = new NeuralNetworkBuilder();
        int prevSize = 0;
        for (int layerSize : layerSizes) {
            if (prevSize != 0) {
                double[] biases = new double[layerSize];
                double[][] weights = new double[layerSize][prevSize];

                for (int i = 0; i < layerSize; i++) {
                    biases[i] = randomGenerator.getRandomDouble() * layerSize;
                    for (int j = 0; j < prevSize; j++) {
                        weights[i][j] = randomGenerator.getRandomDouble();
                    }
                }
                neuralNetworkBuilder.addSigmoidLayer(weights, biases);
            }
            prevSize = layerSize;
        }

        return neuralNetworkBuilder.build();
    }
}
