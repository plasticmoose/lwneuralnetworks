package org.bitbucket.plasticmoose.lwneuralnetworks.utils;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {
    private final long seed;
    private final ThreadLocalRandom random;

    public RandomGenerator(long seed) {
        this.seed = seed;
        random = ThreadLocalRandom.current();
    }

    public double getRandomDouble() {
        return random.nextDouble(-1, 1);
    }
}
