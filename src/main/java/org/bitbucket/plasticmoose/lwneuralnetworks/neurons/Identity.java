package org.bitbucket.plasticmoose.lwneuralnetworks.neurons;

public class Identity implements Neuron {
    private final static Identity instance = new Identity();

    @Override
    public double getOutput(double input) {
        return input;
    }

    public static Identity[] ofSize(int size) {
        Identity[] result = new Identity[size];
        for (int i = 0; i < size; i++) {
            result[i] = instance;
        }

        return result;
    }
}
