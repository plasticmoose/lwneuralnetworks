package org.bitbucket.plasticmoose.lwneuralnetworks.neurons;

public class Perceptron implements Neuron {
    private final int bias;

    public Perceptron(int bias) {
        this.bias = bias;
    }

    @Override
    public double getOutput(double input) {
        if (input > bias) return 1;
        else return 0;
    }
}
