package org.bitbucket.plasticmoose.lwneuralnetworks.neurons;

public interface Neuron {
    double getOutput(double input);
}
