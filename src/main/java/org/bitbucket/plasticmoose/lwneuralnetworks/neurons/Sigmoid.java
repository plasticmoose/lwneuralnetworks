package org.bitbucket.plasticmoose.lwneuralnetworks.neurons;

public class Sigmoid implements Neuron {
    private final double bias;

    public Sigmoid(double bias) {
        this.bias = bias;
    }

    @Override
    public double getOutput(double input) {
        return 1 / (1 + Math.exp(-input - bias));
    }
}
