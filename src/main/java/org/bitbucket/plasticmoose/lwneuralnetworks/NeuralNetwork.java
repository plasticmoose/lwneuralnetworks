package org.bitbucket.plasticmoose.lwneuralnetworks;

public class NeuralNetwork {
    private final Layer[] layers;

    public NeuralNetwork(Layer[] layers) {
        this.layers = layers;
    }

    public double[] getOutput(double[] input) {
        return getOutputRecurrent(input, 0);
    }

    private double[] getOutputRecurrent(double[] input, int layer) {
        if (layer == layers.length) return input;
        else return getOutputRecurrent(layers[layer].getOutput(input), layer + 1);
    }
}
