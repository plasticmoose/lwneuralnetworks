package org.bitbucket.plasticmoose.lwneuralnetworks;

import org.bitbucket.plasticmoose.lwneuralnetworks.neurons.Neuron;

public class Layer {
    private final int previousLayerSize;
    private final int size;
    private final double[][] weights;
    private final Neuron[] neurons;

    public Layer(double[][] weights, Neuron[] neurons) {
        this.weights = weights;
        this.size = weights.length;
        this.previousLayerSize = weights[0].length;
        this.neurons = neurons;
    }

    public double[] getOutput(double[] input) {
        if (input.length != previousLayerSize) throw new RuntimeException("Input size does not matches declared size.");

        double[] output = new double[size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < previousLayerSize; j++) {
                output[i] += weights[i][j] * input[j];
            }
        }

        return applyNeurons(output);
    }

    private double[] applyNeurons(double[] input) {
        double[] result = new double[input.length];
        for (int i = 0; i < input.length; i++) {
            result[i] = neurons[i].getOutput(input[i]);
        }

        return result;
    }

    public int getSize() {
        return size;
    }
}
